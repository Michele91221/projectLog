package models

// GenericResponse struct
// swagger:response genericResponse
type GenericResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// GenericError struct
// swagger:response genericError
type GenericError struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Info    string `json:"info"`
}
