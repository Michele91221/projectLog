package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

// LogEntry struct
type LogEntry struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Action    string    `gorm:"size:255;not null" json:"action"`
	Route     string    `gorm:"size:1000;not null" json:"route"`
	User      string    `gorm:"size:255;not null"`
	UserID    uint32    `gorm:"not null" json:"userId"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"createAt"`
}

// SaveLogEntry function to create a new log entry
func (l *LogEntry) SaveLogEntry(db *gorm.DB) (*LogEntry, error) {
	err := db.Create(&l).Error
	if err != nil {
		return &LogEntry{}, err
	}
	return l, nil
}

//FindAllLog func to retrieve all logs stored in the DB
func (l *LogEntry) FindAllLog(db *gorm.DB) (*[]LogEntry, error) {
	var err error
	logs := []LogEntry{}
	err = db.Debug().Model(&LogEntry{}).Limit(100).Find(&logs).Error
	if err != nil {
		return &[]LogEntry{}, err
	}
	return &logs, err
}

//FindLogByID function to retrieve the log with a specific ID
func (l *LogEntry) FindLogByID(db *gorm.DB, uid uint32) (*LogEntry, error) {
	var err error
	err = db.Debug().Model(LogEntry{}).Where("id = ?", uid).Take(&l).Error
	if err != nil {
		return &LogEntry{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &LogEntry{}, errors.New("Log Not Found")
	}
	return l, err
}
