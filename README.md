# projectLog
Logging example using:
- Golang
- Postgres
- Chi

### How to Run

To run this application you have to:

1. Download the project or clone this repository

    ```
    $ git clone https://github.com/Michele9122/projectLog.git
    ```

2. Start the server with go run .\cmd\user-service\.


3. With your server running, visit the site: `http://localhost:8888`.


### Amazon AWS, Kubernetes, Docker, Prometheus and Grafana

1. I created a new IAM role on Amazon AWS, attached with Amazon EKS policy.
At first I created the VPC for the EKS cluster, then the EKS cluster and finally I generated the Kubernetes worker nodes, all by using CloudFormation.

2. To create the Golang docker container I used a Dockerfile multi-stage build, in order to make the docker image as smaller as possible.

3. I used Prometheus and Grafana to monitor the Kubernetes cluster and the projectLog APIs, such as the users_login_total value.
I created the namespace and I used helm to install Prometheus and Grafana. For Grafana I created a LoadBalancer, which allowed me to have an IP accessible from the outside.

You can find grafana here: 
http://aea2db718cdab11e9a3c402d20353a58-992475759.us-east-2.elb.amazonaws.com/

Username: admin
Password: grafanaadmin

## How It Works
To view the list of users or logs, you have to login or create a new user.
You can reach the API here: 
http://a62a9825eccc411e9a3c402d20353a58-292578725.us-east-2.elb.amazonaws.com:8888/v1/

### POST to create a new user
    - http://a62a9825eccc411e9a3c402d20353a58-292578725.us-east-2.elb.amazonaws.com:8888/v1/user
    - In the body:
            nickname:
            email:
            password:
### POST to login 
    - http://a62a9825eccc411e9a3c402d20353a58-292578725.us-east-2.elb.amazonaws.com:8888/v1/login
    - In the body:
            user: test
            pass: test

After the login:
 - you will get a JWT Token, you have to copy and paste it in the header, as Authorization Bearer TOKENJWT.
 - You can also use the API ENDPOINT to retrieve all the users and logs or search by ID.

## Swagger
Swagger is a WIP, you can find the definition file inside swaggerui folder.

## API ENDPOINTS

### Login
- Path : `/v1/login`
- Method: `GET`
- Fields: `user, pass`
- Response: `200`

### All Users
- Path : `/v1/user`
- Method: `GET`
- Response: `200`

### Create User
- Path : `/v1/user`
- Method: `POST`
- Fields: `nickname, email, password`
- Response: `200`

### Details a User
- Path : `/v1/user/{id}`
- Method: `GET`
- Response: `200`

### All Logs
- Path : `/v1/log`
- Method: `GET`
- Response: `200`

### Details a Log
- Path : `/v1/log/{id}`
- Method: `GET`
- Response: `200`

## Required Packages
- Dependency management
    * [dep](https://github.com/golang/dep)
- ORM (GORM)
    * [Postgres](github.com/jinzhu/gorm)
- Database
    * [Postgres](github.com/jinzhu/gorm/dialects/postgres)
- Routing
    * [chi](https://github.com/go-chi/chi)

