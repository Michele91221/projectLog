package main

import (
	"net/http"

	"github.com/Michele9122/projectLog/models"
	"github.com/Michele9122/projectLog/pkg/server"
	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"
	"golang.org/x/crypto/bcrypt"
)

//liveProbiness function for Kubernetes, to check if the API are UP
func (a *API) liveProbiness(w http.ResponseWriter, r *http.Request) {
	render.JSON(w, r, map[string]interface{}{
		"status": "ok",
	},
	)
}

//readyProbiness function for Kubernetes, to check if the API are UP
func (a *API) readyProbiness(w http.ResponseWriter, r *http.Request) {
	render.JSON(w, r, map[string]interface{}{
		"status": "ok",
	},
	)
}

//login func check the nickname, the password and create a JWT token
func (a *API) login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	loginUser := r.FormValue("user")
	loginPass := r.FormValue("pass")

	if loginUser == "" || loginPass == "" {
		server.SendError(w, r, 400, nil, "invalid user or password")
		return
	}

	user := models.User{}
	err := a.userService.Driver.DB.Debug().Model(models.User{}).Where("nickname = ?", loginUser).Take(&user).Error
	if err != nil {
		server.SendError(w, r, 400, err, "error in user data retrival")
		return
	}

	err = models.VerifyPassword(user.Password, loginPass)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		server.SendError(w, r, 401, err, "bad auth")
		return
	}
	_, tokenString, err := a.tokenAuth.Encode(jwtauth.Claims{
		"userId": user.ID,
		"user":   user.Nickname,
		"email":  user.Email,
	})

	if err != nil {
		server.SendError(w, r, 500, err, "error in token generation")
		return
	}

	a.userService.LoginCounter.Inc()

	render.JSON(w, r, map[string]interface{}{
		"status":  "ok",
		"message": tokenString,
	},
	)
}
