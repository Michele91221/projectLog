package main

import (
	"net/http"

	"github.com/Michele9122/projectLog/models"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"
	"go.uber.org/zap"
)

//LogUserAction middleware to log the user action (Action: GET, POST etc Route: the request URI)
func (a *API) LogUserAction(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer next.ServeHTTP(w, r)
		token, _, _ := jwtauth.FromContext(r.Context())

		claims, ok := token.Claims.(jwt.MapClaims)

		if !ok && !token.Valid {
			return
		}

		log := &models.LogEntry{
			Action: methodToAction(r.Method),
			Route:  r.RequestURI,
			User:   claims["user"].(string),
			UserID: uint32(claims["userId"].(float64)),
		}

		logEntry, err := log.SaveLogEntry(a.userService.Driver.DB)

		if err != nil {
			a.logger.Error("error in logging user action", zap.Error(err))
			return
		}

		a.logger.Info("user action", zap.Any("log", logEntry))
	})
}

func methodToAction(method string) string {
	switch method {
	case "GET":
		return "view"
	case "POST":
		return "insert"
	default:
		return ""
	}
}
