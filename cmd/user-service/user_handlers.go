package main

import (
	"net/http"
	"strconv"

	"github.com/Michele9122/projectLog/models"
	"github.com/Michele9122/projectLog/pkg/server"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

func (a *API) createUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user := models.User{
		Nickname: r.FormValue("nickname"),
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	validationErr := user.Validate()
	if validationErr != nil {
		server.SendError(w, r, 400, validationErr, "error in user data validation")
		return
	}

	usr, err := user.SaveUser(a.userService.Driver.DB)
	if err != nil {
		server.SendError(w, r, 400, err, "error in user save")
		return
	}

	render.JSON(w, r, map[string]interface{}{
		"status":  "ok",
		"message": usr,
	},
	)
}

func (a *API) getUsers(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	users, err := user.FindAllUsers(a.userService.Driver.DB)
	if err != nil {
		server.SendError(w, r, 500, err, "error in getting users")
		return
	}

	render.JSON(w, r, map[string]interface{}{
		"status":  "ok",
		"message": users,
	},
	)
}

//getUserByID func
func (a *API) getUserByID(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	users, err := user.FindUserByID(a.userService.Driver.DB, uint32(id))
	if err != nil {
		server.SendError(w, r, 500, err, "error in getting users")
		return
	}

	render.JSON(w, r, map[string]interface{}{
		"status":  "ok",
		"message": users,
	},
	)
}

func (a *API) getLogs(w http.ResponseWriter, r *http.Request) {
	log := &models.LogEntry{}
	logs, err := log.FindAllLog(a.userService.Driver.DB)
	if err != nil {
		server.SendError(w, r, 500, err, "error in getting logs")
		return
	}

	render.JSON(w, r, map[string]interface{}{
		"status":  "ok",
		"message": logs,
	},
	)
}

//getLogById func
func (a *API) getLogByID(w http.ResponseWriter, r *http.Request) {
	log := &models.LogEntry{}
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	logs, err := log.FindLogByID(a.userService.Driver.DB, uint32(id))
	if err != nil {
		server.SendError(w, r, 500, err, "error in getting log")
		return
	}

	render.JSON(w, r, map[string]interface{}{
		"status":  "ok",
		"message": logs,
	},
	)
}
