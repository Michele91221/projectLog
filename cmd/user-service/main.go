package main

import (
	"os"

	userservice "github.com/Michele9122/projectLog/internal/user-service"
	"github.com/Michele9122/projectLog/pkg/driver"
	"github.com/Michele9122/projectLog/pkg/server"
	"github.com/go-chi/jwtauth"
	"github.com/joho/godotenv"
	"go.uber.org/zap"
)

// API struct represent the service
type API struct {
	logger      *zap.Logger
	userService *userservice.UserService
	tokenAuth   *jwtauth.JWTAuth
}

// @title ProjectLog API
// @version 1.0
// @description This is a sample server celler server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8888
// @BasePath /v1

// @securityDefinitions bearerAuth
// @in header
// @name Authorization

func main() {
	//Create logger instance
	logger, _ := zap.NewProduction()
	logger.Info("init API service")

	//Load envs
	godotenv.Load()

	//DB envs
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASSWORD")
	dbPort := os.Getenv("DB_PORT")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")

	//Server envs
	httpHost := os.Getenv("HTTP_HOST")
	httpPort := os.Getenv("HTTP_PORT")

	//Create db driver
	driver, err := driver.NewPostgresDriver(dbUser, dbPass, dbHost, dbPort, dbName, logger)
	if err != nil {
		logger.Panic("error in database initialization", zap.Error(err))
	}
	userService := userservice.NewUserService(driver, logger)

	tokenAuth := jwtauth.New("HS256", []byte("secret"), nil)

	api := &API{
		logger:      logger,
		userService: userService,
		tokenAuth:   tokenAuth,
	}

	server := server.NewServer(api.routes(), logger)
	server.Start(httpHost, httpPort)
}
