package userservice

import (
	"github.com/Michele9122/projectLog/models"
	"github.com/Michele9122/projectLog/pkg/driver"
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
)

// UserService struct
type UserService struct {
	logger       *zap.Logger
	Driver       *driver.PostgresDriver
	LoginCounter prometheus.Counter
}

// NewUserService inizialization with PostgresDriver, Zapper and Prometheus with loginCounter
func NewUserService(driver *driver.PostgresDriver, logger *zap.Logger) *UserService {
	// ORM database migration
	driver.DB.Debug().AutoMigrate(&models.User{}, &models.LogEntry{})

	loginCounter := prometheus.NewCounter(prometheus.CounterOpts{
		Name: "users_login_total",
		Help: "Number of successful logins",
	})
	prometheus.MustRegister(loginCounter)

	logger.Info("postgres model database migration done")

	return &UserService{
		logger:       logger,
		Driver:       driver,
		LoginCounter: loginCounter,
	}
}
