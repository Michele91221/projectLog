package driver

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // postgres dialect
	"go.uber.org/zap"
)

// PostgresDriver struct
type PostgresDriver struct {
	logger *zap.Logger
	DB     *gorm.DB
}

// NewPostgresDriver create
func NewPostgresDriver(user, pass, host, port, name string, logger *zap.Logger) (*PostgresDriver, error) {
	connection := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", host, port, user, name, pass)
	db, err := gorm.Open("postgres", connection)
	if err != nil {
		return nil, err
	}

	logger.Info("postgres driver ready")

	return &PostgresDriver{
		logger: logger,
		DB:     db,
	}, nil
}
