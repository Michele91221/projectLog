package middleware

import (
	"net/http"

	"github.com/Michele9122/projectLog/pkg/server"
	"github.com/go-chi/jwtauth"
)

// Authenticator custom middleware
func Authenticator(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, _, err := jwtauth.FromContext(r.Context())

		if err != nil {
			server.SendError(w, r, 401, nil, "token not found")
			return
		}

		if token == nil || !token.Valid {
			server.SendError(w, r, 401, nil, "token not valid")
			return
		}

		// Token is authenticated, pass it through
		next.ServeHTTP(w, r)
	})
}
