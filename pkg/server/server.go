package server

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"go.uber.org/zap"
)

// Server struct
type Server struct {
	logger *zap.Logger
	router *chi.Mux
}

// NewServer funct
func NewServer(router *chi.Mux, logger *zap.Logger) *Server {
	return &Server{
		logger: logger,
		router: router,
	}
}

// Start function
func (s *Server) Start(host, port string) error {
	s.logger.Info("Serving HTTP server", zap.String("host", host), zap.String("port", port))
	err := http.ListenAndServe(fmt.Sprintf("%s:%s", host, port), s.router)
	return err
}
