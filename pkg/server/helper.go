package server

import (
	"net/http"

	"github.com/go-chi/render"
)

// SendError helper function
func SendError(w http.ResponseWriter, r *http.Request, statusCode int, err error, message string) {
	w.WriteHeader(statusCode)
	render.JSON(w, r, map[string]interface{}{
		"status":  "error",
		"info":    err,
		"message": message,
	},
	)
}
